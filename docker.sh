#!/bin/bash


# This script runs a container and executes a command inside it.
# Builds docker image if necessary

set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`
DOCKER_OTHER_COMMANDS="${DOCKER_OTHER_COMMANDS:=""}"

prepare_docker_base_image()
{
  docker build --file $SCRIPT_PATH/Dockerfile --tag builder_base $SCRIPT_PATH
}

get_project_name()
{
  # if PROJECT_NAME not specified, get the one from git repository
  if [ -z "$PROJECT_NAME" ]
  then
    # this folder should be a submodule, so main repo is one folder above
    PROJECT_NAME=`basename -s .git \`cd ${SCRIPT_PATH}/../ && git config --get remote.origin.url\``
  fi
  echo "Project" $PROJECT_NAME
}


prepare_docker_project_image()
{
  get_project_name
  IMAGE_PROJECT_NAME="${PROJECT_NAME}_builder"
  echo $IMAGE_PROJECT_NAME
  # set dockerfile specific for this project, if not yet set
  DOCKERFILE_PER_PROJECT="${DOCKERFILE_PER_PROJECT:-${SCRIPT_PATH}/../builder_per_project/Dockerfile}"
  # check if it really exists
  if test -f "$DOCKERFILE_PER_PROJECT"; then
    docker build \
    --file $DOCKERFILE_PER_PROJECT \
    --tag $IMAGE_PROJECT_NAME  \
    --build-arg USER_ID=$(id -u ${USER}) \
    --build-arg GROUP_ID=$(id -g ${USER}) \
    $SCRIPT_PATH
  else
    echo Dockerfile per project does not exist!
    # build base as a project docker
    docker build --file $SCRIPT_PATH/Dockerfile --tag $IMAGE_PROJECT_NAME $SCRIPT_PATH
  fi
}

# GUI app from another user (root) from docker
#https://www.baeldung.com/linux/docker-container-gui-applications
#$touch /tmp/.docker.xauth  
#$xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f /tmp/.docker.xauth nmerge - 
run_docker_interactive()
{
  echo Interactive
  docker run \
    --rm \
    --interactive \
    --cap-add=SYS_PTRACE \
    --security-opt seccomp=unconfined \
    --tty \
    --mount type=bind,source="${SCRIPT_PATH}/../",target=/workspace \
    --name "`whoami`_${PROJECT_NAME}" \
    --hostname `whoami` \
    --network="host"  \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="${HOME}/.Xauthority",target=/root/.Xauthority \
    --env DISPLAY \
    --privileged \
    --volume /tmp/.docker.xauth:/tmp/.docker.xauth\
    --volume /tmp/.X11-unix:/tmp/.X11-unix  \
    --env XAUTHORITY=/tmp/.docker.xauth\
    $IMAGE_PROJECT_NAME \
    /bin/bash
  }

run_docker_command()
{
  echo Command only
  TIMESTAMP=`date +"%F_%H-%M-%S"`
  rm --force $SCRIPT_PATH/docker.cid
  docker run \
    --rm \
    --cidfile=$SCRIPT_PATH/docker.cid \
    --mount type=bind,source="${SCRIPT_PATH}/../",target=/workspace \
    --name "`whoami`_${PROJECT_NAME}_${TIMESTAMP}" \
    --hostname `whoami` \
    --user $(id -u):$(id -g) \
    --network="host"  \
    --env DISPLAY \
    --privileged \
    $DOCKER_OTHER_COMMANDS \
    $IMAGE_PROJECT_NAME \
    $@
}


echo Got params: $@

prepare_docker_base_image
prepare_docker_project_image
if [[ $# -eq 0 ]] ;
then
  run_docker_interactive
else
  run_docker_command $@
fi

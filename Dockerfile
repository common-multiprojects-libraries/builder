FROM ubuntu:20.04



ENV TZ=Europe/Warsaw
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# keep as small number of RUN commands as possible
RUN apt-get update -y &&  apt-get install -y \
  ctags \
  build-essential \
  wget \
  git \
  && rm -rf /var/lib/apt/lists/*

WORKDIR "/workspace"

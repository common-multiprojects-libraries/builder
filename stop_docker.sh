#!/bin/bash

set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`
DOCKER_ID=`cat $SCRIPT_PATH/docker.cid` || DOCKER_ID=""

if [ -z "$DOCKER_ID" ]
then
  echo "no saved container is running"
else
  docker stop $DOCKER_ID
  rm $SCRIPT_PATH/docker.cid
  echo "Stopped!"
fi
